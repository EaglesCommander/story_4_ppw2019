from django.shortcuts import render
from django.http import HttpResponse

def index(request):
	return render (request, 'homepage.html')

def contacts(request):
    return render (request, 'contacts.html')

def about(request):
    return render (request, 'about_me.html')

def projects(request):
    return render (request, 'projects.html')

def secret(request):
    return render (request, 'secret.html')
